<?php
require_once 'config/config.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once 'components/head.php';
    ?>
</head>

<body>

    <!-- Top Bar -->
    <?php
    require_once 'components/top-bar.php';
    ?>
    <!-- End Top Bar -->

    <!-- ======= Header ======= -->
    <?php
    require_once 'components/header.php';
    ?>
    <!-- End Header -->

    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center" style="background-image: url('');">
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Metode Pembayaran</h2>
                            <p>Kamu masih bingung metode pembayaran apa saja yang dapat digunakan saat order joki tugas di Buatin Tugasku? Jangan khawatir, ada banyak metode pembayaran kok! Pembayaran dapat dilakukan melalui Transfer Bank (BCA) dan E-Wallet (Dana, Gopay, dan Shopeepay)</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="<?= $base_url ?>">Home</a></li>
                        <li>Metode Pembayaran</li>
                    </ol>
                </div>
            </nav>
        </div><!-- End Breadcrumbs -->

        <section id="clients" class="sample-page clients">
            <div class="container">

                <div class="clients-slider swiper">
                    <div class="swiper-wrapper align-items-center">
                        <div class="swiper-slide"><img src="<?= $base_url ?>assets/img/metode-pembayaran/bca.png" class="img-fluid" alt=""></div>
                        <div class="swiper-slide"><img src="<?= $base_url ?>assets/img/metode-pembayaran/dana.png" class="img-fluid" alt=""></div>
                        <div class="swiper-slide"><img src="<?= $base_url ?>assets/img/metode-pembayaran/gopay.png" class="img-fluid" alt=""></div>
                        <div class="swiper-slide"><img src="<?= $base_url ?>assets/img/metode-pembayaran/shopeepay.png" class="img-fluid" alt=""></div>
                    </div>
                </div>

            </div>
        </section>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <?php
    require_once 'components/footer.php';
    ?>
    <!-- End Footer -->

</body>

</html>