<?php
require_once 'config/config.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once 'components/head.php';
    ?>
</head>

<body>

    <!-- Top Bar -->
    <?php
    require_once 'components/top-bar.php';
    ?>
    <!-- End Top Bar -->

    <!-- ======= Header ======= -->
    <?php
    require_once 'components/header.php';
    ?>
    <!-- End Header -->

    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center" style="background-image: url('');">
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Refferal</h2>
                            <p>Coming Soon.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="<?= $base_url ?>">Home</a></li>
                        <li>Program Refferal</li>
                    </ol>
                </div>
            </nav>
        </div><!-- End Breadcrumbs -->

        <section class="sample-page">
            <div class="container" data-aos="fade-up">

                <p>
                    COMING SOON
                </p>

            </div>
        </section>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <?php
    require_once 'components/footer.php';
    ?>
    <!-- End Footer -->

</body>

</html>