<?php
require_once 'config/config.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once 'components/head.php';
    ?>
</head>

<body>

    <!-- Top Bar -->
    <?php
    require_once 'components/top-bar.php';
    ?>
    <!-- End Top Bar -->

    <!-- ======= Header ======= -->
    <?php
    require_once 'components/header.php';
    ?>
    <!-- End Header -->

    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center" style="background-image: url('');">
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Daftar Layanan</h2>
                            <p>Berikut ini adalah daftar layanan yang kami sediakan.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="<?= $base_url ?>">Home</a></li>
                        <li>Daftar Layanan</li>
                    </ol>
                </div>
            </nav>
        </div><!-- End Breadcrumbs -->

        <section id="services" class="sample-page services">
            <div class="container" data-aos="fade-up">

                <div class="row gy-4" data-aos="fade-up" data-aos-delay="100">

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-activity"></i>
                            </div>
                            <h3>Pembuatan Skripsi</h3>
                            <p>
                                Daftar Skripsi :
                            </p>
                            <ul>
                                <li>Teknik Informatika</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-broadcast"></i>
                            </div>
                            <h3>Tugas Makalah</h3>
                            <p>Makalah adalah karya tulis pelajar atau mahasiswa sebagai laporan hasil pelaksanaan tugas sekolah atau perguruan tinggi.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-easel"></i>
                            </div>
                            <h3>Desain Grafis</h3>
                            <p>Desain grafis atau rancang grafis adalah proses komunikasi menggunakan elemen visual, seperti tipografi, fotografi, serta ilustrasi yang dimaksudkan untuk menciptakan persepsi akan suatu pesan yang disampaikan.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-bounding-box-circles"></i>
                            </div>
                            <h3>Tugas Bahasa Inggris</h3>
                            <p>Bahasa Inggris adalah bahasa Jermanik yang pertama kali dituturkan di Inggris pada Abad Pertengahan Awal dan saat ini merupakan bahasa yang paling umum digunakan di seluruh dunia.</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-calendar4-week"></i>
                            </div>
                            <h3>Program Dasar</h3>
                            <p>
                                Daftar Program Dasar :
                            </p>
                            <ul>
                                <li>Pascal</li>
                                <li>C/C#/C++</li>
                                <li>HTML + CSS + JS</li>
                                <li>Java</li>
                                <li>PHP</li>
                                <li>Python</li>
                                <li>Visual Basic</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-chat-square-text"></i>
                            </div>
                            <h3>Program Lanjutan</h3>
                            <p>
                                Daftar Program Lanjutan :
                            </p>
                            <ul>
                                <li>Laravel</li>
                                <li>CodeIgniter</li>
                                <li>NodeJS</li>
                                <li>Android Studio</li>
                                <li>React/Vue/Angular JS</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-chat-square-text"></i>
                            </div>
                            <h3>Service HP Android/IOS</h3>
                            <p>
                                Daftar Layanan :
                            </p>
                            <ul>
                                <li>Perbaikan LCD</li>
                                <li>Perbaikan Tombol Volume/Power</li>
                                <li>Repair Bootloop</li>
                                <li>Bypass Google Account</li>
                                <li>Bypass Cloud Account (micloud, oppo cloud)</li>
                                <li>Bypass Icloud</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-chat-square-text"></i>
                            </div>
                            <h3>Service Laptop/Macbook</h3>
                            <p>
                                Daftar Layanan :
                            </p>
                            <ul>
                                <li>Install Ulang</li>
                                <li>Fix Corrupt Data</li>
                                <li>Clear Virus</li>
                                <li>Install Aplikasi/Software</li>
                                <li>Cek Kerusakan Hardware</li>
                                <li>Tambah/Ganti SSD/RAM</li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <?php
    require_once 'components/footer.php';
    ?>
    <!-- End Footer -->

</body>

</html>