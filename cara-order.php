<?php
require_once 'config/config.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once 'components/head.php';
    ?>
</head>

<body>

    <!-- Top Bar -->
    <?php
    require_once 'components/top-bar.php';
    ?>
    <!-- End Top Bar -->

    <!-- ======= Header ======= -->
    <?php
    require_once 'components/header.php';
    ?>
    <!-- End Header -->

    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <div class="breadcrumbs">
            <div class="page-header d-flex align-items-center" style="background-image: url('');">
                <div class="container position-relative">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-6 text-center">
                            <h2>Bagaimana Cara Ordernya ?</h2>
                            <p>Kamu masih bingung bagaimana cara order jasa joki pembuatan tugas di JOKIINAJA? Jangan khawatir! Kamu bisa menggunakan jasa joki pengerjaan tugas kami dengan sangat mudah dan cepat. Silahkan baca tutorial di bawah.</p>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <div class="container">
                    <ol>
                        <li><a href="<?= $base_url ?>">Home</a></li>
                        <li>Cara Order</li>
                    </ol>
                </div>
            </nav>
        </div><!-- End Breadcrumbs -->

        <section id="faq" class="sample-page faq">
            <div class="container" data-aos="fade-up">



                <div class="row gy-4">

                    <div class="col-lg-12">

                        <div class="accordion accordion-flush" id="faqlist" data-aos="fade-up" data-aos-delay="100">

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1">
                                        <span class="num">1.</span>
                                        Chat Kami di Whatsapp
                                    </button>
                                </h3>
                                <div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        WA: 0881 082 023 998
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-2">
                                        <span class="num">2.</span>
                                        Kirimkan Detail Tugas
                                    </button>
                                </h3>
                                <div id="faq-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Kirim foto tugas kamu beserta detail dan deadlinenya
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-3">
                                        <span class="num">3.</span>
                                        Lakukan Pembayaran
                                    </button>
                                </h3>
                                <div id="faq-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Pembayaran bisa melalui transfer bank / e-wallet
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-4">
                                        <span class="num">4.</span>
                                        Menunggu Hasil Tugas
                                    </button>
                                </h3>
                                <div id="faq-content-4" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Tim kami akan segera menyelesaikan tugas kamu
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-5">
                                        <span class="num">5.</span>
                                        Tugas Kamu Selesai
                                    </button>
                                </h3>
                                <div id="faq-content-5" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Tim kami akan mengirimkan hasil tugas kamu
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-6">
                                        <span class="num">6.</span>
                                        Berapa lama waktu pengerjaan tugasnya ya?
                                    </button>
                                </h3>
                                <div id="faq-content-6" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Tidak perlu khawatir! Waktu pengerjaan tugas bisa disesuaikan dengan kebutuhan kamu dan deadlinenya juga nih.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-7">
                                        <span class="num">7.</span>
                                        Metode pembayaran tugas bisa melalui apa saja ya ?
                                    </button>
                                </h3>
                                <div id="faq-content-7" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Mudah sekali! Pembayaran di Buatin Tugasku dapat dilakukan melalui transfer bank dan e-wallet ya.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-8">
                                        <span class="num">8.</span>
                                        Bagaimana cara menghubungi Buatin Tugasku jika ingin order jasanya?
                                    </button>
                                </h3>
                                <div id="faq-content-8" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Kamu bisa menghubungi dan tanya-tanya langsung ke kami melalui WhatsApp 0881 082 023 998. Hubungi kami sekarang!
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </section>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <?php
    require_once 'components/footer.php';
    ?>
    <!-- End Footer -->

</body>

</html>