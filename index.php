<?php
require_once 'config/config.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    require_once 'components/head.php';
    ?>
</head>

<body>
    <!-- Top Bar -->
    <?php
    require_once 'components/top-bar.php';
    ?>
    <!-- End Top Bar -->

    <!-- ======= Header ======= -->
    <?php
    require_once 'components/header.php';
    ?>
    <!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero" class="hero">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">
                <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2>Joki Tugas Murah, Cepat & Terpercaya</h2>
                    <p>Melayani Pembuatan Tugas Program, Desain Grafis, Bahasa Inggris, Skripsi</p>
                    <div class="d-flex justify-content-center justify-content-lg-start">
                        <a href="<?= $base_url ?>whatsapp" class="btn-get-started">Free Konsultasi</a>
                        <!-- <a href="https://www.youtube.com/watch?v=LXb3EKWsInQ" class="glightbox btn-watch-video d-flex align-items-center"><i class="bi bi-play-circle"></i><span>Watch Video</span></a> -->
                    </div>
                </div>
                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="<?= $base_url ?>assets/img/hero-img.svg" class="img-fluid" alt="" data-aos="zoom-out" data-aos-delay="100">
                </div>
            </div>
        </div>

        <div class="icon-boxes position-relative">
            <div class="container position-relative">
                <div class="row gy-4 mt-5">

                    <div class="col-xl-6 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="icon-box">
                            <!-- <div class="icon"><i class="bi bi-geo-alt"></i></div> -->
                            <h4 class="title text-light">Privasi Terjaga 100%</h4>
                            <p>
                                JOKIINAJA telah berkomitmen untuk selalu menjaga dan meningkatkan keamanan serta privasi pelanggan dalam hal apapun secara jelas dan tegas.
                            </p>
                        </div>
                    </div>

                    <div class="col-xl-6 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="icon-box">
                            <!-- <div class="icon"><i class="bi bi-geo-alt"></i></div> -->
                            <h4 class="title text-light">Layanan Cepat 12 Jam</h4>
                            <p>
                                Buatin Tugasku akan membantu mengatasi permasalahan tugasmu serta memberikan konsultasi secara cepat dan tepat selama 12 jam melalui <a href="<?= $base_url ?>whatsapp" class="text-light">WhatsApp</a>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        </div>
    </section>
    <!-- End Hero Section -->

    <main id="main">

        <!-- ======= Our Services Section ======= -->
        <section id="services" class="services sections-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Daftar Layanan</h2>
                    <p>Layanan yang tersedia untuk saat ini</p>
                </div>

                <div class="row gy-4" data-aos="fade-up" data-aos-delay="100">

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item  position-relative">
                            <div class="icon">
                                <i class="bi bi-activity"></i>
                            </div>
                            <h3>Pembuatan Skripsi</h3>
                            <p>
                                Daftar Skripsi :
                            </p>
                            <ul>
                                <li>Teknik Informatika</li>
                            </ul>
                        </div>
                    </div><!-- End Service Item -->

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-broadcast"></i>
                            </div>
                            <h3>Tugas Makalah</h3>
                            <p>Makalah adalah karya tulis pelajar atau mahasiswa sebagai laporan hasil pelaksanaan tugas sekolah atau perguruan tinggi.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-easel"></i>
                            </div>
                            <h3>Desain Grafis</h3>
                            <p>Desain grafis atau rancang grafis adalah proses komunikasi menggunakan elemen visual, seperti tipografi, fotografi, serta ilustrasi yang dimaksudkan untuk menciptakan persepsi akan suatu pesan yang disampaikan.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-bounding-box-circles"></i>
                            </div>
                            <h3>Tugas Bahasa Inggris</h3>
                            <p>Bahasa Inggris adalah bahasa Jermanik yang pertama kali dituturkan di Inggris pada Abad Pertengahan Awal dan saat ini merupakan bahasa yang paling umum digunakan di seluruh dunia.</p>
                        </div>
                    </div><!-- End Service Item -->

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-calendar4-week"></i>
                            </div>
                            <h3>Program Dasar</h3>
                            <p>
                                Daftar Program Dasar :
                            </p>
                            <ul>
                                <li>Pascal</li>
                                <li>C/C#/C++</li>
                                <li>HTML + CSS + JS</li>
                                <li>Java</li>
                                <li>PHP</li>
                                <li>Python</li>
                                <li>Visual Basic</li>
                            </ul>
                        </div>
                    </div><!-- End Service Item -->

                    <div class="col-lg-4 col-md-6">
                        <div class="service-item position-relative">
                            <div class="icon">
                                <i class="bi bi-chat-square-text"></i>
                            </div>
                            <h3>Program Lanjutan</h3>
                            <p>
                                Daftar Program Lanjutan :
                            </p>
                            <ul>
                                <li>Laravel</li>
                                <li>CodeIgniter</li>
                                <li>NodeJS</li>
                                <li>Android Studio</li>
                                <li>React/Vue/Angular JS</li>
                            </ul>
                        </div>
                    </div><!-- End Service Item -->

                </div>

            </div>
        </section>
        <!-- End Our Services Section -->

        <!-- ======= Frequently Asked Questions Section ======= -->
        <section id="faq" class="faq">
            <div class="container" data-aos="fade-up">

                <div class="row gy-4">

                    <div class="col-lg-4">
                        <div class="content px-xl-5">
                            <h3>Daftar Pertanyaan</h3>
                            <p>
                                Kamu ingin menggunakan jasa joki pengerjaan tugas di JOKIIANAJA tetapi masih ragu dan bingung? Berikut adalah daftar pertanyaan yang paling sering ditanyakan oleh pelanggan kami, jika pertanyaan kamu tidak ada di daftar, jangan khawatir! kamu dapat menanyakannya langsung ke kami!
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-8">

                        <div class="accordion accordion-flush" id="faqlist" data-aos="fade-up" data-aos-delay="100">

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-1">
                                        <span class="num">1.</span>
                                        Apakah privasi dan keamanan pelanggan terjaga ?
                                    </button>
                                </h3>
                                <div id="faq-content-1" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Tidak perlu khawatir! Buatin Tugasku selalu mengutamakan privasi dan keamanan pelanggan, dijamin 100% tidak akan ketahuan guru atau dosennya ya!
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-2">
                                        <span class="num">2.</span>
                                        Apakah hasil tugas dijamin memuaskan?
                                    </button>
                                </h3>
                                <div id="faq-content-2" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Hasil tugas kami jamin 100% memuaskan ya, karena dikerjakan langsung oleh tim profesional di bidangnya dan kami menyediakan revisi tugas ya jika ada yang kurang atau ingin ditambah.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-3">
                                        <span class="num">3.</span>
                                        Apakah bisa revisi hasil tugas?
                                    </button>
                                </h3>
                                <div id="faq-content-3" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Bisa banget ya! Buatin Tugasku menyediakan layanan revisi hasil tugas selama 1x24 jam sejak penyerahan hasil tugas ya.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-4">
                                        <span class="num">4.</span>
                                        Apakah joki tugas [nama tugas] bisa kak ?
                                    </button>
                                </h3>
                                <div id="faq-content-4" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Pertama-tama kamu harus cek dulu nih apakah tugas kamu ada di daftar tugas kami, jika ada maka sudah pasti bisa. Tapi jika tidak ada kamu masih bisa menghubungi kami melalui instagram atau whatsapp untuk menanyakan terkait tugas kamu ya, karena daftar tugas adalah tugas-tugas unggulan yang banyak diminati pelanggan kami saja.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-5">
                                        <span class="num">5.</span>
                                        Apakah bisa joki tugas dengan deadline yang mepet?
                                    </button>
                                </h3>
                                <div id="faq-content-5" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Bisa banget yaa, tetapi minimal 12 jam sebelum deadline pengumpulan tugas yaa, karena tim kami juga butuh waktu untuk mengerjakan tugas kamu nih.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-6">
                                        <span class="num">6.</span>
                                        Berapa lama waktu pengerjaan tugasnya ya?
                                    </button>
                                </h3>
                                <div id="faq-content-6" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Tidak perlu khawatir! Waktu pengerjaan tugas bisa disesuaikan dengan kebutuhan kamu dan deadlinenya juga nih.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-7">
                                        <span class="num">7.</span>
                                        Metode pembayaran tugas bisa melalui apa saja ya ?
                                    </button>
                                </h3>
                                <div id="faq-content-7" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Mudah sekali! Pembayaran di Buatin Tugasku dapat dilakukan melalui transfer bank dan e-wallet ya.
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-item">
                                <h3 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#faq-content-8">
                                        <span class="num">8.</span>
                                        Bagaimana cara menghubungi Buatin Tugasku jika ingin order jasanya?
                                    </button>
                                </h3>
                                <div id="faq-content-8" class="accordion-collapse collapse" data-bs-parent="#faqlist">
                                    <div class="accordion-body">
                                        Kamu bisa menghubungi dan tanya-tanya langsung ke kami melalui WhatsApp 0881 082 023 998. Hubungi kami sekarang!
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </section>
        <!-- End Frequently Asked Questions Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact sections-bg">
            <div class="container" data-aos="fade-up">

                <div class="section-header">
                    <h2>Contact</h2>
                </div>

                <div class="row gx-lg-0 gy-4">

                    <div class="col-lg-4">

                        <div class="info-container d-flex flex-column align-items-center justify-content-center">
                            <div class="info-item d-flex">
                                <i class="bi bi-geo-alt flex-shrink-0"></i>
                                <div>
                                    <h4>Location:</h4>
                                    <p>Indonesia</p>
                                </div>
                            </div>

                            <div class="info-item d-flex">
                                <i class="bi bi-envelope flex-shrink-0"></i>
                                <div>
                                    <h4>Email:</h4>
                                    <p>admin@jokiinaja.my.id</p>
                                </div>
                            </div>

                            <div class="info-item d-flex">
                                <i class="bi bi-phone flex-shrink-0"></i>
                                <div>
                                    <h4>Call:</h4>
                                    <p>+62 881 082 023 998</p>
                                </div>
                            </div>

                            <div class="info-item d-flex">
                                <i class="bi bi-clock flex-shrink-0"></i>
                                <div>
                                    <h4>Open Hours:</h4>
                                    <p>Mon-Sat: 09AM - 22PM</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-8">
                        <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                                </div>
                                <div class="col-md-6 form-group mt-3 mt-md-0">
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                            </div>
                            <div class="form-group mt-3">
                                <textarea class="form-control" name="message" rows="7" placeholder="Message" required></textarea>
                            </div>
                            <div class="my-3">
                                <div class="loading">Loading</div>
                                <div class="error-message"></div>
                                <div class="sent-message">Your message has been sent. Thank you!</div>
                            </div>
                            <div class="text-center"><button type="submit">Send Message</button></div>
                        </form>
                    </div><!-- End Contact Form -->

                </div>

            </div>
        </section><!-- End Contact Section -->

    </main>
    <!-- End #main -->

    <!-- ======= Footer ======= -->
    <?php
    require_once 'components/footer.php';
    ?>
    <!-- End Footer -->

</body>

</html>