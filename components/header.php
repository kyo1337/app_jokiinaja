<header id="header" class="header d-flex align-items-center">

    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
        <a href="<?= $base_url ?>" class="logo d-flex align-items-center">
            <!-- Uncomment the line below if you also wish to use an image logo -->
            <!-- <img src="assets/img/logo.png" alt=""> -->
            <h1>JOKIINAJA<span>.</span></h1>
        </a>
        <nav id="navbar" class="navbar">
            <ul>
                <li><a href="<?= $base_url ?>">Home</a></li>
                <li><a href="<?= $base_url ?>cara-order">Cara Order</a></li>
                <li><a href="<?= $base_url ?>metode-pembayaran">Metode Pembayaran</a></li>
                <li><a href="<?= $base_url ?>daftar-layanan">Daftar Layanan</a></li>
                <li><a href="<?= $base_url ?>refferal">Program Refferal</a></li>
            </ul>
        </nav>

        <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
        <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

    </div>
</header>